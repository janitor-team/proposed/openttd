12.2 (2022-04-02)
------------------------------------------------------------------------
Feature: Remember the last-used signal between games (#9792)
Change: [MacOS] Allow touchbar usage on all supported OS versions (#9776)
Change: Add a timestamp in name of crash files (#9761)
Fix #9736: Duplicate multiplayer window opens upon canceling password entry (#9842)
Fix: Removing long roads doesn't prioritise refusal of local authority over other errors (#9831)
Fix #9020: Glitchy station coverage highlight when changing selection (#9825)
Fix: Correct some Romanian town names (#9819)
Fix: Original music playback rate was slightly too fast (#9814)
Fix #9811: Use the NewGRF-defined vehicle center when dragging ships and aircraft (#9812)
Fix: Do not let shares in the company taking over another company disappear (#9808)
Fix #9802: Crash when using lots of NewGRF waypoint types (#9803)
Fix #9766: Don't write uninitialised data in config file (#9767)
Fix #9743: [MacOS] Don't try to render touchbar sprites with invalid zoom level (#9776)
Fix #9774: Building roadstop in estimation mode updates station acceptance (#9775)
Fix: If vehicles only refit to cargo-slots >= 32, the default cargo was wrong (#9744)
Fix #9735: Possible desync when replacing a depot on same tile (#9738)
Fix #9730: [Network] Connections can use an invalid socket due to a race condition (#9731)
Fix: Don't show sign edit window for GS-owned signs (#9716)
Fix #9702: Display order window for vehicle group on ctrl-click only when using shared orders (#9704)
Fix #9680: Crash when loading really old savegames with aircraft in certain places (#9699)
Fix: Update last servicing dates when using the date cheat (#9694)
Fix: Error message shows about missing glyphs while suitable fallback font is found (#9692)


